//outputs for s3 bucket for cloudtrail
output "cloudtrail-logs-bucket_domain_name" {
  value = "${aws_s3_bucket.cloudtrail-logs.bucket_domain_name}"
}

output "cloudtrail-logs-bucket_id" {
  value = "${aws_s3_bucket.cloudtrail-logs.id}"
}

output "cloudtrail-logs-bucket_arn" {
  value = "${aws_s3_bucket.cloudtrail-logs.arn}"
}
