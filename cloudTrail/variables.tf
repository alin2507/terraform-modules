
variable "name" {
  type = "string"
  default = "devopsteam-tech-cloudtrail"
  description = "the cloudtrail name"
}

variable "s3_bucket_name" {
  type = "string"
  default = "devopsteam-tech-cloudtrail-logs"
  description = "the name of the bucket where cloudtrail logs will  be stored"
}

variable "include_global_service_events" {
  default = true
}