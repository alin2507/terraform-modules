
resource "aws_cloudtrail" "aws-cloudtrail" {
  name                          = "${var.name}"
  s3_bucket_name                = "${aws_s3_bucket.cloudtrail-logs.id}"
  include_global_service_events = "${var.include_global_service_events}"
}


//bucket for cloudtrail
resource "aws_s3_bucket" "cloudtrail-logs" {
  bucket        = "${var.s3_bucket_name}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::devopsteam-tech-cloudtrail-logs"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::devopsteam-tech-cloudtrail-logs/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}
